import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/home/index.vue"),
  },
  {
    path: "/button",
    name: "Button",
    component: () => import("../views/button/index.vue"),
  },
  {
    path: "/alert",
    name: "Alert",
    component: () => import("../views/alert/index.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
