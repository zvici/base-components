## API

PROPERTIES

```
block {
    type: Boolean,
    default: false,
    description: Renders a 100% width button (expands to the width of its parent container)
},
disabled {
    type: Boolean,
    default: false,
    description: When set to `true`, disables the component's functionality and places it in a disable state, and I have a change in tailwind.config.js add opacity: ["disabled"],
},
loading {
    type: Boolean,
    default: false
}
pill {
    type: Boolean,
    default: false,
    description: Renders the button with the pill style appearance when set to 'true'
},
size{
    type: String,
    default: 'md',
    description: Set the size of the component's appearance. 'sm', 'md' (default), or 'lg'
},
squared {
    type: Boolean,
    default: false,
    description: Renders the button with non-rounded corners when set to 'true'
},
to {
    type: Object or String,
    default:  ,
    description: <router-link> prop: Denotes the target route of the link. When clicked, the value of the to prop will be passed to `router.push()` internally, so the value can be either a string or a Location descriptor object
},
type {
    type: String,
    default: 'button',
    description: The value to set the button's 'type' attribute to. Can be one of 'button', 'submit' 'reset'
},
variant {
    type: String,
    default: 'secondary',
    description: Change color theme button
}
```

```
SLOTS {
    name: default,
    description: content to place in the button
}
```

```
EVENTS {
    event: click,
    arguments: Native click event object,
    description: Emitted when non-disabled button clicked
}
```
