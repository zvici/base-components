API

```
Properties
dismissible {
    type: Boolean,
    default: false,
    description: When set, enables the dismiss close button
},
show {
    type: Boolean or Number or String,
    default: false,
    description: When set, shows the alert. Set to a number (seconds) to show and automatically dismiss the alert after the number of seconds has elapsed
},
variant {
    type: String,
    default: 'info',
    description: Applies one of the theme color variants to the component
},
```

```
v-model
{
    property: show,
    event: Input
}
```
